#!/usr/bin/env perl

use strict;
use warnings;

#use MIME::Base64 qw< encode_base64 >;

sub slurp {
    open my $fh, "<", $_[0] or die $!;
    local $/;
    return scalar <$fh>;
}

my $template = slurp($ARGV[0]);
my $content  = do { local $/; scalar <STDIN> };

$template =~ s#__IFINDEX__(.*?)__ENDIF__#
  my $str = $1;
  $ARGV[1] =~ /index/ ? $str : "";
#egs;

$template =~ s#__TITLE__#
  $ARGV[1] =~ /index/ ? "" :
  $ARGV[1] =~ /research/ ? ": Research" :
  $ARGV[1] =~ /teaching/ ? ": Teaching" :
  $ARGV[1] =~ /outreach/ ? ": Outreach" :
  $ARGV[1] =~ /programming/ ? ": Programming" : die;
#eg;

$template =~ s#__NUM__#
  $ARGV[1] =~ /index/ ? 1 :
  $ARGV[1] =~ /research/ ? 2 :
  $ARGV[1] =~ /teaching/ ? 3 :
  $ARGV[1] =~ /outreach/ ? 4 :
  $ARGV[1] =~ /programming/ ? 5 : die;
#eg;

$template =~ s#__WIDTH__(.*?)__ENDWIDTH__#
  `identify assets/\Q$1\E | cut -d' ' -f3 | cut -dx -f1 | tr -d '\n'`;
#eg;

$template =~ s/^(\s*)__CONTENT__/"$1" . join "\n$1", split "\n", $content/egm;

# Experiment with inlining small images; doesn't have a noticeable effect.
#$template =~ s#<img src="(.*?)"#
#  -s "$1" < 3000
#    ? "<img src=\"data:@{[`file --mime-type -b \Q$1\E | tr -d '\n'`]};base64,@{[join '', split \"\n\", encode_base64(slurp($1))]}\""
#    : "<img src=\"$1\"";
##eg;

print $template;
