#!/usr/bin/env perl

while(<>) {
    s/~/ /g;
    s/\\E/𝓔/g;
    s/\\Os/𝓞~Spec(*A*)~/g;
    s/\\O/𝓞/g;
    s/\\models/⊨/g;
    s/\\varphi/φ/g;
    s#\[([\w ]+)\|([\w ]+?)\]#
        my $title = $2;
        my $url   = $1;
        $url =~ s/([^\w])/sprintf '%%%x', ord $1/eg;
        "[$title](https://ncatlab.org/nlab/show/$url)";
    #eg;
    print;
}
