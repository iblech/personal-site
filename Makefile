SOURCES=$(wildcard src/*.md)

SHELL=/usr/bin/env bash -o pipefail

all: build $(patsubst src/%.md,build/%.html,$(SOURCES))

build: $(wildcard assets/*)
	mkdir -p build
	cp -r assets build
	cp -r toplevel/* build

build/%.html: src/%.md template.html fill-template.pl preprocess.pl
	./preprocess.pl $< | nix-shell -p pandoc --command "pandoc -t html5" | ./fill-template.pl template.html $< > $@

clean:
	rm -rf build

upload: all
	rsync -avz build/ root@speicherleck.de:/var/www/iblech/

.PHONY: clean all upload
