# Programming

I'm an advocate of free-as-in-speech software and Creative Commons.

* [Slides for a general audience (in German)](https://www.speicherleck.de/iblech/stuff/fross.pdf)


## Pugs

Many moons ago, I contributed to
[Pugs](https://en.wikipedia.org/wiki/Pugs_(programming)), an experimental
compiler and interpreter for [Perl 6](https://perl6.org/). This project had a
lasting influence on me, among other things introducing me to
[Haskell](https://www.haskell.org/).

* [A retrospective on Pugs](https://rawgit.com/iblech/talk-pugs-retrospective/master/slides.pdf)


## Free software projects I maintain

* [sshlatex](https://github.com/iblech/sshlatex): A collection of hacks to
  efficiently run LaTeX via ssh
* [nat-traverse](https://www.speicherleck.de/iblech/nat-traverse/):
  NAT gateway traversal utility
* [instiki-cli](https://github.com/iblech/instiki-cli):
  Tiny tool to edit Instiki wikis such as the [nLab](https://ncatlab.org/nlab/show/HomePage) locally
* [A Haskell library for constructive algebra](https://github.com/iblech/constructive-algebra)
  (student project, exact computation with complex numbers and computation of Galois groups)


## Functional programming

I regularly give talks (in German) at the [Curry Club
Augsburg](http://www.curry-club-augsburg.de/), a group of people interested in
functional programming. Topics included:

* [Haskell](https://rawgit.com/iblech/vortrag-haskell/master/folien.pdf)
* [Monoids, free monoids and monads as monoids](http://curry-club-augsburg.de/posts/2015-07-25-ankuendigung-sechstes-treffen.html)
* [Effect systems](http://curry-club-augsburg.de/posts/2015-09-19-ankuendigung-achtes-treffen.html)
* [Seemingly impossible functional programs](http://curry-club-augsburg.de/posts/2015-11-09-neuntes-treffen.html)
* [Initial algebras and terminal coalgebras](http://curry-club-augsburg.de/posts/2016-05-17-fuenfzehntes-treffen.html)
* [Infinite-time Turing machines](http://curry-club-augsburg.de/posts/2016-09-06-neunzehntes-treffen.html)
* [Continuations](http://curry-club-augsburg.de/posts/2017-01-01-dreiundzwanzigstes-treffen.html)
* [Dark corners of logic](http://curry-club-augsburg.de/posts/2017-09-07-dreissigstes-treffen.html)
* [Zipper](http://curry-club-augsburg.de/posts/2018-03-22-sechsunddreissigstes-treffen.html)


## Ancient talks

[Ancient talks for the Linux User Group Augsburg](https://www.speicherleck.de/iblech/index-ancient.xhtml) (in German)
