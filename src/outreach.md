# Outreach

## For school students

I'm a funding member of the [Matheschülerzirkel
Augsburg](https://www.math.uni-augsburg.de/schueler/mathezirkel/). School
students aged between 9 and 19 who are interested in mathematics can
participate in biweekly mathematical seminars, in monthly written
correspondence and in our yearly mathematics camp.

We invite everyone who's interested in creating such a program to [contact
us](mailto:mathezirkel@math.uni-augsburg.de). We can provide a huge corpus of
worked-out materials and share our experience in contacting schools.

Here are some of my written notes for the Matheschülerzirkel (in German),
primarily aimed at students aged 15 to 19. More notes are [available on
GitHub](https://github.com/iblech/mathezirkel-kurs/) (all [CC
BY-SA](https://creativecommons.org/licenses/by-sa/4.0/), feel free to change
and use them for your purposes).

* [Surreal numbers](https://rawgit.com/iblech/mathezirkel-kurs/master/thema02-surreale-zahlen/blatt02.pdf)
* [Pseudorandomness with elliptic curves](https://rawgit.com/iblech/mathezirkel-kurs/master/thema03-kryptographie/blatt04.pdf)
* [Synthetic differential geometry](https://rawgit.com/iblech/mathezirkel-kurs/master/thema05-sdg/blatt05.pdf)
* [Gödel's incompleteness theorem](https://rawgit.com/iblech/mathezirkel-kurs/master/thema11-goedel/skript.pdf)
* [Analytic number theory](https://rawgit.com/iblech/mathezirkel-kurs/master/thema12-analytische-zahlentheorie/skript.pdf)
* [The residue theorem in complex analysis](https://rawgit.com/iblech/mathezirkel-kurs/master/thema18-residuensatz/skript.pdf)

Here is an interactive environment for toying with randomness using Python (in English):

* [Experimenting with randomness in the browser](https://www.speicherleck.de/iblech/zufall-im-browser/index.en.html)

I also give yearly courses at the
[JGW-SchülerAkademie](https://jgw-ev.de/schuelerakademie/):

* 2015: Mathematik der Vorhersagen (joint with Philipp Wacker)
* 2016: Wie ward Licht? Mathematische Simulationen physikalischer Systeme
  (joint with Meru Alagalingam)
* 2017: Wenn die Sonne über Monte-Carlo scheint: wie mathematische
  Zufallsmethoden Simulationen physikalischer Naturphänomene ermöglichen (joint
  with Maximilian Schlögel)
* 2018: Mathematik auf den Spuren von Ada Lovelace: die Berechnung der Natur
  und das Wesen von Berechnung (joint with Matthias Hutzler)
* 2019: Mathematische Phantome – weil sie versprechen, Wunder für uns zu
  wirken, lassen wir sie existieren (joint with Matthias Hutzler)


## For general audiences (selection)

* [Bizarre logico-philosophical tales on the axiom of choice](https://www.speicherleck.de/iblech/stuff/37c3-axiom-of-choice.pdf) (37th Chaos Communication Congress)
* [Die wundersame Welt der vierdimensionalen Geometrie](https://www.youtube.com/watch?v=d19zmiVBLS8)
  (Christmas lecture in Augsburg, joint with [Matthias Hutzler](https://github.com/MatthiasHu/))
* [Faith in mathematics](https://rawgit.com/iblech/mathezirkel-kurs/master/superturingmaschinen/faith-in-mathematics.pdf)
  (34th Chaos Communication Congress)
* [How does artificial intelligence accomplish the feat of learning?](https://rawgit.com/iblech/mathematik-der-vorhersagen/master/vortrag-34c3/slides.pdf)
  (34th Chaos Communication Congress)
* The cosmic distance ladder: measuring distances in space
  (Sternstunden der Wissenschaft im Planetarium Augsburg)
* [The physics of sounds](https://www.tuebix.org/2016/programm/ingo-blechschmidt-physik-der-klaenge-experimente-mit-linux-bordmitteln/)
  (Tübinger Linux-Tag 2016)
* [Fun with infinitely large numbers](https://www.youtube.com/watch?v=wZzn4INtbwY&list=PLwpepnYDFK9OxuqUvLDQPoTBEGyuj4iLU)
  (33th Chaos Communication Congress)
* [The curious world of four-dimensional geometry](https://www.youtube.com/watch?v=ct0_g1amEpw&list=PLwpepnYDFK9OxuqUvLDQPoTBEGyuj4iLU&index=2)
  (33rd Chaos Communication Congress, joint with Matthias Hutzler)
* [The secret of the number five](https://rawgit.com/iblech/number5/master/talk-32c3.pdf)
  (32rd Chaos Communication Congress)
