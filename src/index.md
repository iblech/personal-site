<div style="
  padding: 0.5em;
  font-size: x-large;
  text-shadow: 0px 0px 20px #ffffff;
  margin-bottom: 1.5em;
  text-align: center;
  box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
  border-top: 0.35em solid #ab7340;
  border-bottom: 0.35em solid #ab7340;
">
  <p>Enough injectives with just Zorn's lemma and not the axiom of choice:<br>
  <a style="font-weight: bold" href="https://www.speicherleck.de/iblech/stuff/paper-flabby-objects.pdf">draft paper</a>
  (see appendix)</a></p>
</div>

<div style="
  padding: 0.5em;
  font-size: x-large;
  text-shadow: 0px 0px 20px #ffffff;
  margin-bottom: 1.5em;
  text-align: center;
  box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
  border-top: 0.35em solid #1b7340;
  border-bottom: 0.35em solid #1b7340;
">
  <p>ABMV 2024:
  <a href="https://www.speicherleck.de/iblech/stuff/slides-abmv2024.pdf">slides</a> &bull;
  <a href="https://www.speicherleck.de/iblech/stuff/early-draft-modal-multiverse.pdf">rough early draft of a preprint</a> &bull;
  <a href="https://www.speicherleck.de/iblech/stuff/slides-herrsching2023.pdf">slides on constructive forcing</a> &bull;
  <a href="https://raw.githubusercontent.com/iblech/constructive-maximal-ideals/master/tex/extended.pdf">notes on constructive forcing</a> (Section 4)
  </p>

  <p><a href="https://rt.quasicoherent.io/">2024 Verona Minicourse on
  extracting programs from proofs</a></p>

  <p>HoTTEST 2023:
  <a href="https://www.speicherleck.de/iblech/stuff/slides-hottest2023.pdf">slides</a>
  (<a href="https://agdapad.quasicoherent.io/%7eAgdaPadova/html/maximal-ideals-as-convenient-fictions.html">Agda code</a>)
  </p>

  <p>Herrsching 2023:
  <a href="https://www.speicherleck.de/iblech/stuff/slides-herrsching2023.pdf">slides</a></p>

  Niš:
  <a href="https://www.speicherleck.de/iblech/stuff/slides-nis2023.pdf">slides</a><br>
  Fischbachau:
  <a href="https://www.speicherleck.de/iblech/stuff/slides-fischbachau2022.pdf">slides</a>,
  <a href="https://agdapad.quasicoherent.io/%7eAgdaPadova/">Agda exercises</a><br>
  <a href="https://www.speicherleck.de/iblech/stuff/slides-brixen2022a.pdf">plenary</a><br>
  Slides for Brixen 2022:
  <a href="https://www.speicherleck.de/iblech/stuff/slides-brixen2022b.pdf">informal</a>,
  <a href="https://www.speicherleck.de/iblech/stuff/slides-brixen2022a.pdf">plenary</a><br>
  <a href="https://agdapad.quasicoherent.io/%7eAgdaPadova/html/talk-schlehdorf.html">Slides for Schlehdorf 2022</a><br>
  <a href="https://www.speicherleck.de/iblech/stuff/slides-dagstuhl2021.pdf">Slides for Dagstuhl 2021</a><br>
  <a href="https://www.speicherleck.de/iblech/stuff/slides-antwerpen2022.pdf">Slides for Antwerp 2022</a><br>
  <a href="https://www.speicherleck.de/iblech/stuff/slides-swansea2022.pdf">Slides for CiE 2022</a><br>
  <a href="https://www.speicherleck.de/iblech/stuff/slides-luminy2023.pdf">Slides for CIRM 2023</a> (<a href="https://www.speicherleck.de/iblech/stuff/early-draft-modal-multiverse.pdf">rough early draft of a preprint</a>)
</div>


# About me

I'm a mathematician working in applied topos theory. I obtained my PhD in
October 2017 under the supervision of
[Marc Nieper-Wißkirchen](https://www.math.uni-augsburg.de/prof/alg/Arbeitsgruppe/Nieper-Wisskirchen/)
at the [University of Augsburg](https://math.uni-augsburg.de/). Besides
[research](research.html), I love [teaching mathematics at all
levels](teaching.html) and am passionate about [doing mathematics with school
students](outreach.html). Currently I'm back in Augsburg. During the academic year 2018/2019, I was working at the [Università di
Verona](https://www.di.univr.it/) under the supervision of [Peter
Schuster](http://www.di.univr.it/?ent=persona&id=21404&lang=en). In the winter
term 2017/2018, I was substituting a junior professor at the University of
Augsburg, and during the summer of 2018, I was a guest at the [Max Planck
Institute for Mathematics in the Sciences](https://www.mis.mpg.de/) in Leipzig.

I explore applications of the internal language of toposes, particularly in
commutative algebra and in algebraic geometry.

* **[Short research summary](research.html)**
* **[Research statement](assets/research-statement.pdf)**
* **[PhD thesis: Using the internal language of toposes in algebraic
  geometry](https://rawgit.com/iblech/internal-methods/master/notes.pdf)**
  (large portions require only familiarity with scheme theory, not with topos
  theory;
  [slides](https://rawgit.com/iblech/internal-methods/master/slides-duesseldorf2017.pdf),
  [more slides](https://github.com/iblech/internal-methods),
  [2015 talk at the IHÉS](https://www.youtube.com/watch?v=7S8--bIKaWQ))

<hr>

* **[Paper: An elementary and constructive proof of Grothendieck's generic freeness lemma](https://arxiv.org/abs/1807.01231)**
* **[Paper: Flabby and injective objects in toposes](https://arxiv.org/abs/1810.12708)**
* **[Short note: A constructive Knaster–Tarski proof of the uncountability of the reals](https://arxiv.org/abs/1902.07366)** (joint with Matthias Hutzler)
* **[Paper: Exploring mathematical objects from custom-tailored toposes](https://arxiv.org/abs/2204.00948)**
* **[Preprint: Reflections on reflection for intuitionistic set theories](https://rawgit.com/iblech/internal-methods/master/paper-reflection.pdf)** (joint with Andrew Swan)
* **[Paper: A general Nullstellensatz for generalized spaces](https://rawgit.com/iblech/internal-methods/master/paper-qcoh.pdf)**
* **[Expository paper: Generalized spaces for constructive algebra](https://arxiv.org/abs/2012.13850)**
* **[Paper: Maximal ideals in countable rings, constructively](https://arxiv.org/abs/2207.03873)** (joint with Peter Schuster)
* **[Paper: A constructive picture of Noetherian conditions and well quasi-orders](https://link.springer.com/chapter/10.1007/978-3-031-36978-0_5)** (joint with Gabriele Buriola and Peter Schuster)
* **[Expository paper: A primer to the set-theoretic multiverse philosophy](https://iblech.gitlab.io/bb/multiverse.html)**
* **[Paper: Reifying dynamical algebra: maximal ideals in countable rings, constructively](https://github.com/iblech/constructive-maximal-ideals/blob/main/tex/extended.pdf?raw=true)** (joint with Peter Schuster)

<hr>

* UNILOG talk: **[Exploring the internal language of toposes](https://rawgit.com/iblech/internal-methods/master/slides-unilog2018.pdf)**
* Como talk: **[How topos theory can help commutative algebra](https://rawgit.com/iblech/internal-methods/master/slides-como2018.pdf)** (for topos theorists)
* Leipzig talk: **[How topos theory can help algebra and geometry](https://rawgit.com/iblech/internal-methods/master/slides-leipzig2018.pdf)** (for a general mathematical audience)
* Colloquium Logicum/Münchenwiler/Padova talk: **[New reduction techniques in commutative algebra driven by logical methods](https://rawgit.com/iblech/internal-methods/master/slides-padova2018.pdf)**
* PSSL104 talk: **[How not to constructivize cohomology](https://rawgit.com/iblech/internal-methods/master/slides-pssl104.pdf)** (also see [paper](https://rawgit.com/iblech/internal-methods/master/paper-flabby-objects.pdf))
* 6WFTop/CT2019 talk: **[A general Nullstellensatz for generalised spaces](https://rawgit.com/iblech/internal-methods/master/slides-edinburgh2019.pdf)** (also see [rough draft](https://rawgit.com/iblech/internal-methods/master/paper-qcoh.pdf))
* Dagstuhl 2021 talk: **[Bridging the foundational gap: Updating algebraic geometry in face of current challenges regarding formalizability, constructivity and predicativity](https://rawgit.com/iblech/internal-methods/master/slides-dagstuhl2021.pdf)**

I contribute to the [nLab](https://ncatlab.org/nlab/show/HomePage) and to the
[Stacks Project](https://stacks.math.columbia.edu/), and encourage everyone to
consider [Eugenia Cheng's manifesto for inclusivity in category theory and
mathematics at large](http://eugeniacheng.com/inclusivity/).

**Recent results by Matthias Ritter (formerly Matthias Hutzler):** [The infinitesimal topos classifies the
theory of quotients of local algebras by nilpotent
ideals](https://gitlab.com/MatthiasHu/master-thesis/raw/master/thesis.pdf?inline=false)
(master's thesis), [Syntactic presentations for glued toposes
and for crystalline
toposes](https://gitlab.com/MatthiasHu/dissertation/-/jobs/artifacts/master/raw/dissertation.pdf?job=pdf)
(PhD thesis)

**Recent result by Johannes Riebel:** [The undecidability of BB(748)](assets/bachelor-thesis-undecidability-bb748.pdf)

## Travel plans

* April 2018:
  [PSSL 103](https://www.math.muni.cz/%7Eloregianf/PSSL103/PSSL103.php) in Brno
* April 2018:
  [Oberseminar Mathematische Logik](https://www.mathematik.uni-muenchen.de/%7Eschwicht/seminars/osem/indexosem.php)
  in Munich
* April 2018:
  [Computational Approaches to the Foundations of Mathematics](http://www.cas.uni-muenchen.de/veranstaltungen/tagungen/ws_sanders_schuster_et_al/index.html)
  in Munich
* May 2018:
  [Summer School on Types, Sets and
  Constructions](https://www.him.uni-bonn.de/programs/future-programs/future-trimester-programs/types-sets-constructions/summer-school/)
  in Bonn
* May 2018:
  [Philosophy of mathematics: objects, structures, and logics](https://filmatnetwork.com/filmat2018/)
  in Mussomeli
* June 2018:
  [UniLog](https://www.uni-log.org/start6.html) in Vichy
* June 2018:
  [Toposes in Como](http://tcsc.lakecomoschool.org/) in Como
* July 2018:
  [Jugendschülerakademie](https://jgw-ev.de/schuelerakademie/) in Papenburg
* August 2018:
  [Mathematics camp](https://www.math.uni-augsburg.de/schueler/mathezirkel/veranstaltungen/) in Violau
* September 2018:
  [Colloquium Logicum 2018](https://www.cl2018.uni-bayreuth.de/en/index.html) in Bayreuth
* September 2018:
  [Proof and Computation](https://www.mathematik.uni-muenchen.de/%7Eschwicht/pc18.php) in Fischbachau
* October 2018: [PSSL 104](https://mysite.science.uottawa.ca/phofstra/PSSL18.html) in Amsterdam
* October 2018: [EUTYPES2018](https://cs.au.dk/research/logic-and-semantics/eutypes2018/) in Aarhus
* October 2018: [Münchenwiler Meeting](https://mw.inf.unibe.ch/) in Münchenwiler
* February 2019: [Motivic Verona](http://profs.sci.univr.it/~angeleri/minicourses%202018.html) in Verona
* March 2019:
  [Compact course on proof interpretations](https://logicseminarverona.wordpress.com/2019/02/20/compact-course/) in Verona
* April 2019: [Workshop on Formal Topology](https://www.cs.bham.ac.uk/~sjv/6WFTop/) in Birmingham
* Spring 2019: Warwick
* July 2019: [Category Theory 2019](http://conferences.inf.ed.ac.uk/ct2019/)
* September 2019: [Proof and Computation](https://www.mathematik.uni-muenchen.de/~schwicht/pc19.php) in Herrsching


<div style="text-align: center">
  <img width="454" height="379" src="assets/external-internal.jpeg" style="max-width: 100%; padding-top: 1em" alt="External mathematician vs. internal mathematician"><br>
  Illustration: Carina Willbold (CC BY-SA)
</div>
