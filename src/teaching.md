# Teaching

## (Synthetic) algebraic geometry / algebraic function fields

Course in the winter term 2024 at the University of Antwerp.
[The lecture notes, exercise sheets and other course material is here.](https://aff.quasicoherent.io/)

Also: [Antwerp Logic Adventures](https://www.antwerp-logic-adventures.be/)


## Sheaves and logic

My course on [sheaves and logic](https://gitlab.com/iblech/garben-und-logik) in
the winter term 2017/2018 was recorded.

* **[Recordings of the lectures](https://www.youtube.com/playlist?list=PLR-3Jx6BfhkgjagoGM3Z15G0j5hv5i66v)**
  (in German)
* **[Lecture notes](https://iblech.gitlab.io/garben-und-logik/notes.pdf)** (in English)
* **[Exercise sheets](https://iblech.gitlab.io/garben-und-logik/uebungen.pdf)** (in German)


## Lecture notes for pizza seminars

I organized several [pizza seminars](https://pizzaseminar.speicherleck.de/),
seminars by students for students. Lecture notes and exercise sheets are
available in German:

* **[Category theory](https://pizzaseminar.speicherleck.de/skript1/pizzaseminar.pdf)**
  (with a focus on motivating examples)
* **[Constructive mathematics](https://pizzaseminar.speicherleck.de/skript2/konstruktive-mathematik.pdf)**
  (including a discussion of Hilbert's program and the Bohr topos)

Related: **[QED course on metamathematics and topos theory](https://qed.quasicoherent.io/)** (mostly in German)


## Construction, realisability, double negation

**[Master minicourse at the University of Verona](https://rt.quasicoherent.io/)**


## Expository notes

* [A quickstart guide to derived functors](https://rawgit.com/iblech/talk-homological-algebra/master/notes-derived-functors.pdf)
* [A one-page self-contained proof of a baby version of Kaplansky's theorem without Noetherian hypotheses](https://rawgit.com/iblech/talk-homological-algebra/master/kaplansky-en.pdf)
  (a module is finitely generated and projective if and only if it is locally
  finite free)
* [Fun with the little Zariski topos](https://pizzaseminar.speicherleck.de/skript2/zariski-topos-klein.pdf) (in German)
* [The Picard group and the Riemann–Roch theorem](https://pizzaseminar.speicherleck.de/picard-riemann-roch/notizen.pdf) (in German)
* [Higher direct images for dummies](https://rawgit.com/iblech/higher-direct-images/master/directlua.pdf)
  (joint with Pascuale Zenobio de Rossi, work in progress)
* [On the Jordan canonical form](https://www.speicherleck.de/iblech/stuff/tutor-la-ii-quast/jordan.pdf)
  (in German)
* [A primer on automatic differentiation](https://pizzaseminar.speicherleck.de/automatic-differentiation/notes.pdf)
* [Modules over principal ideal domains: the Smith normal form approach](https://rawgit.com/iblech/talk-homological-algebra/master/smith.pdf)


## Talks for general mathematical or compsci audiences

* [Exploring hypercomputation with the effective topos](https://rawgit.com/iblech/mathezirkel-kurs/master/superturingmaschinen/slides-warwick2017.pdf)
  (Joint Philosophy/Mathematics Seminar in Warwick)
* [The double negation translation and the CPS transformation](https://rawgit.com/iblech/talk-constructive-mathematics/master/negneg-translation.pdf)
  (Computer Science Seminar at the KU Leuven)
* [Konstruktive Mathematik, die Doppelnegationsübersetzung und Continuations](https://rawgit.com/iblech/talk-constructive-mathematics/master/hal2015-notes.pdf)
  (HAL2015)
* [Introduction to homotopy type theory](https://rawgit.com/iblech/talk-hott/master/slides.pdf)


## Previous courses

Exercise sheets for courses where I coordinated the tutorials (in German):

* [Galois theory I & II](https://github.com/iblech/algebra)
* [Homological algebra I & II](https://github.com/iblech/homologische-algebra)
* [Model categories](https://rawgit.com/iblech/modellkategorien/master/leaderboard.html)
* [Commutative algebra](http://algebra.speicherleck.de/)
* [Algebraic number theory](https://github.com/iblech/algebraische-zahlentheorie)
* [Complex analysis](https://gitlab.com/iblech/funktionentheorie)


## Teaching statement

My [teaching statement](assets/teaching-statement.pdf) is online. I'd love to hear
about your favorite teaching strategies [by
mail](mailto:iblech@speicherleck.de).
